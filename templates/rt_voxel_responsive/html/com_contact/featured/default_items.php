<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.framework');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

// Create a shortcut for params.
$params = &$this->item->params;
?>

<?php if (empty($this->items)) : ?>
	<p> <?php echo JText::_('COM_CONTACT_NO_CONTACTS'); ?>	 </p>
<?php else : ?>
		<?php $numberOfItems = count($this->items); ?>
		<?php $count = 0; ?>
  			<?php foreach ($this->items as $item) : ?>
  				<div class="contactinfo">
                  <?php if ($this->params->get('show_email_headings')) : ?>
                  <?php echo $item->email_to; ?><br />
                  <?php endif; ?>
                  
                  <?php if ($this->params->get('show_mobile_headings')) : ?>
                  Cell: <a href="tel:<?php echo $item->mobile; ?>"><?php echo $item->mobile; ?></a><br />
                  <?php endif; ?>
                  
                  <?php if ($this->params->get('show_telephone_headings')) : ?>
                  Office: <a href="tel:<?php echo $item->telephone; ?>"><?php echo $item->telephone; ?></a><br />
                  <?php endif; ?>
                  
                  <?php if ($this->params->get('show_fax_headings')) : ?>
                  Fax: <a href="tel:<?php echo $item->fax; ?>"><?php echo $item->fax; ?></a><br />
                  <?php endif; ?>
  				</div>
				<div>
                  <p>
                    <img src="<?php echo htmlspecialchars($item->image); ?>" alt="<?php echo $item->name; ?>"  style="width: 129px; float: left; margin-right: 15px; margin-bottom: 10px;" />
                    <span style="font-size: x-large;">
                        <?php echo $item->name; ?>
                    </span><br /><br />
                    <?php echo $item->misc; ?>
                  </p>
				</div><div style="clear: left;">
					<?php $count++; ?>
					<?php if ($count != $numberOfItems) : ?>
						<hr>
					<?php endif; ?>
				<br /></div>
			<?php endforeach; ?>
<?php endif; ?>
