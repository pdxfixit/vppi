<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2014 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;

class AtsystemFeatureBlockinstall extends AtsystemFeatureAbstract
{
	protected $loadOrder = 200;

	/**
	 * Is this feature enabled?
	 *
	 * @return bool
	 */
	public function isEnabled()
	{
		if (!F0FPlatform::getInstance()->isBackend())
		{
			return false;
		}

		return ($this->cparams->getValue('blockinstall', 0) > 0);
	}

	/**
	 * Block access to the extensions installer
	 */
	public function onAfterInitialise()
	{
		$option = $this->input->getCmd('option', '');

		if (!in_array($option, array('com_installer', 'com_plugins')))
		{
			return;
		}

		$jlang = JFactory::getLanguage();
		$jlang->load('lib_joomla', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('lib_joomla', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
		$jlang->load('lib_joomla', JPATH_ADMINISTRATOR, null, true);

		$blockSetting = $this->cparams->getValue('blockinstall', 0);

		$user = JFactory::getUser();

		if (!$user->guest)
		{
			// Joomla! 1.6 -- Only Super Users have the core.admin global privilege
			$coreAdmin = $user->authorise('core.admin');

			$coreAdmin = (!empty($coreAdmin) && ($coreAdmin === true));

			if (($blockSetting == 1) && ($coreAdmin))
			{
				return;
			}

			if (version_compare(JVERSION, '3.0', 'ge'))
			{
				throw new Exception(JText::_('JGLOBAL_AUTH_ACCESS_DENIED'), 403);
			}
			else
			{
				JError::raiseError(403, JText::_('JGLOBAL_AUTH_ACCESS_DENIED'));
			}
		}
	}
} 