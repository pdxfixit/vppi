<?php
// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');

define('ADMINTOOLS_VERSION', '3.2.1');
define('ADMINTOOLS_DATE', '2014-09-30');
define('ADMINTOOLS_PRO','1');